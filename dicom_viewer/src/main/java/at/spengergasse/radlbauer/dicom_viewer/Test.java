package at.spengergasse.radlbauer.dicom_viewer;

import java.io.File;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;

public class Test 
{

	public static void main(String[] args) throws Exception 
	{
		DicomInputStream input 
			= new DicomInputStream(new File("IM-0001-0018.dcm"));
		DicomObject dcm = input.readDicomObject();
		String name = dcm.getString(Tag.PatientName);
		System.out.println(name);
		
		String patientID = dcm.getString(Tag.PatientID);
		String studyInstanceUID 
			= dcm.getString(Tag.StudyInstanceUID);
		String seriesInstanceUID = 
				dcm.getPrivateCreator(Tag.SeriesInstanceUID);
		String sopInstanceUID = 
				dcm.getString(Tag.SOPInstanceUID);
		
		
		input.close();
	}

}
