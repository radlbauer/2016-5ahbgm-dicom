package at.spengergasse.radlbauer.dicom_viewer.data;

import java.util.ArrayList;
import java.util.List;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

public class Database 
{
	private List<Patient> patients;
	
	public Database()
	{
		patients = new ArrayList<Patient>();
	}
	
	public void add(DicomObject dcm)
	{
		// read patient id from this Dicom-Object
		String patientID = dcm.getString(Tag.PatientID);

		// search for this patient in patient list
		Patient p = null;
		for (Patient p1 : patients)
		{
			if (patientID.equals(p1.getPatientID()))
				p = p1;
		}
		if (p == null)
		{	// patient not found --> create a new one
			p = new Patient(patientID);
			patients.add(p);
		}
		p.add(dcm);
		
	}

	public List<Patient> getPatients() {
		return patients;
	}
}
