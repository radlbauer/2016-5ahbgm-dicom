package at.spengergasse.radlbauer.dicom_viewer.data;

import java.util.ArrayList;
import java.util.List;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

public class Patient 
{
	private String patientID;
	private String name;
	
	private List<Study> studies;
	
	public Patient(String patientID)
	{
		this.patientID = patientID;
		studies = new ArrayList<Study>();
	}
	
	public void add(DicomObject dcm)
	{
		// read study instance uid from dicom object
		String studyInstanceUID 
			= dcm.getString(Tag.StudyInstanceUID);
		Study s = null;
		// search for study with this instance UID
		for (Study s1 : studies)
		{
			if(studyInstanceUID.equals(s1.getStudyInstanceUID()))
			{
				s = s1;
			}
		}
		if (s == null)
		{
			// not found --> create new study with this id
			s = new Study(studyInstanceUID);
			studies.add(s);	
		}
		
		//s.add(dcm);
		// read additional attributes
		name = dcm.getString(Tag.PatientName);
		
			
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatientID() {
		return patientID;
	}

	public List<Study> getStudies() {
		return studies;
	}


	
	
	

}
