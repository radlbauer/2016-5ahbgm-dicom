package at.spengergasse.radlbauer.dicom_viewer.data;

public class Study 
{
	private String studyInstanceUID;

	public Study(String studyInstanceUID) 
	{
		this.studyInstanceUID = studyInstanceUID;
	}

	public String getStudyInstanceUID() {
		return studyInstanceUID;
	}
	
	

}
